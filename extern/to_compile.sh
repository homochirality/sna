# Download resources
wget 'https://www.usna.edu/CS/qepcadweb/INSTALL/qepcad-B.1.69.tar.gz'
wget 'https://www.usna.edu/CS/qepcadweb/INSTALL/saclib2.2.6.tar.gz'
wget 'https://www.usna.edu/CS/qepcadweb/SLFQ/simplify-1.20.tar.gz'

# Compiling

export saclib="$PWD/saclib2.2.6"
export CC=gcc
tar -xzf saclib2.2.6.tar.gz
cd "$saclib/bin"
./sconf
./mkproto
./mkmake
./mklib all
cd ../..

export qe="$PWD/qesource"
tar -xzf qepcad-B.1.69.tar.gz
cd qesource
# TODO: modify qesource/source/userint/HELPFRD.c file to prevent it to load any file,
# comment qe line from qesource/extensions/rend/PLOT_2D_CAD.cc
# and qesource/source/main/BEGINQEPCAD.c
sed -i 's#/bin/csh#/bin/bash#' Makefile
make
cd ..

tar -xzf simplify-1.20.tar.gz
cd simplify
make
cd ..

cp simplify/slfq .
