#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

def main(config, model_path):
    from code.reactions_parser import ReactionsDetails # , sna_matrices

    # Sympy pretty printing, used to print results in LaTeX format
    #from sympy import init_session
    #init_session()

    modelname, species, reactions = load_model(model_path)

    if config['Andres&Carolina Analysis']['enable']:
        rd = ReactionsDetails( reactions, species, sort_by='andres&carolina' )

        reactions_andrescarolina = rd.get_reactions_andrescarolina()
        # detecting if reactions were all of the right type (productive, ...) and had dual pairs
        isAndresCarolina = len(reactions_andrescarolina['others']) == 0

        if isAndresCarolina:
            print("!! Attention! The order of the entered reactions has changed to follow the order:")
            print("!! productive, destructive, autocatalytic, decomposition, enantio-selective")
            print("!! and collision")
        else:
            print("!! Attention! Algorithm Andres&Carolina will not be executed (look below for more info)")

        print()

    # creating matrices without andrescarolina analysis or the list of reactions doesn't fulfill an
    # andrescarolina chemical network
    if not config['Andres&Carolina Analysis']['enable'] or not isAndresCarolina:
        # creating all matrices, leaving reactions list in the same order it was entered
        #S, K, R, xs, ks = sna_matrices(reactions, species)
        rd = ReactionsDetails( reactions, species, sort_by=None )

    print_modelname(modelname)
    print()
    print_model_details(rd)

    if config['Traditional SNA Analysis']['enable']:
        print("\n============================= ( - 1 - ) =============================")
        import code.analyses.traditional as traditional
        traditional.run_analysis(rd, config['Traditional SNA Analysis']['only 2 species for jacobian'])

    if config['Belgian Analysis']['enable']:
        print("\n============================= ( - 2 - ) =============================")
        import code.analyses.belgian as belgian
        belgian.run_analysis(rd)

    if config['Andres&Carolina Analysis']['enable']:
        print("\n============================= ( - 3 - ) =============================")
        import code.analyses.andrescarolina as andrescarolina
        andrescarolina.run_analysis(rd, reactions_andrescarolina)

    if config['Andres&Carolina Analysis 2']['enable']:
        print("\n============================= ( - 4 - ) =============================")
        import code.analyses.andrescarolina2 as andrescarolina2
        andrescarolina2.run_analysis(rd)

    if config['SNA + andrescarolina criterion']['enable']:
        print("\n============================= ( - 5 - ) =============================")
        import code.analyses.sna_andrescarolina as sna_andrescarolina
        n_samples = config['SNA + andrescarolina criterion']['n_samples']
        samples_folder = config['SNA + andrescarolina criterion']['samples_folder']
        sna_andrescarolina.run_analysis(rd, n_samples, samples_folder, modelname)

    print_modelname(modelname)


def load_model(model_file):
    # Opening file where the model is saved
    model_str = model_file.read()

    # Loading model
    try:
        model = {}
        exec(model_str, {}, model)
    except Exception as e:
        print("Model file is faulty, there may be a comma missing")
        print(e)
        exit(1)

    # Verifying model's consistency
    allstr = lambda l: all( isinstance(s, str) for s in l )
    assert "modelname" in model, "`modelname` is missing on the model file"
    assert "species"   in model, "`species` is missing on the model file"
    assert "reactions" in model, "`reactions` is missing on the model file"
    assert isinstance( model["modelname"], str ), "`modelname` must be a string"
    assert isinstance( model["species"], list )   and allstr( model["species"] ),   "`species` must be a list of strings"
    assert isinstance( model["reactions"], list ) and allstr( model["reactions"] ), "`reactions` must be a list of strings"

    # extracting model's data
    return (model["modelname"], model["species"], model["reactions"])

def print_modelname(modelname):
    print("*"*(len(modelname)+10))
    print("*** ", modelname, " ***")
    print("*"*(len(modelname)+10))

def print_model_details(rd):
    import pprint
    import sympy
    import numpy as np

    S, K, R, species, reactions = rd.S, rd.K, rd.R, rd.species, rd.get_reactions()
    nS, nR = S.shape # number of species and number of reactions

    ### Printing model's details and matrices ###
    print("Species:")
    print(species)
    print("Reactions list:")
    pprint.pprint(reactions)

    print("Stoichiometric Matrix:")
    print(S, end="\n\n")
    print("Reactions Order Matrix:")
    print(K, end="\n\n")
    print("Velocity Function:")
    sympy.pprint(R)
    print()

    print("Differential equations functions (polynomials) vector:")
    sympy.pprint(S*R)
    print()

    if S.shape[0] != np.linalg.matrix_rank(S):
        print("Warning: Stoichiometric Matrix is SINGULAR!")
        print()

# code to use with args parser, gotten from https://stackoverflow.com/a/43357954
def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__ == '__main__':
    import argparse
    from argparse import FileType
    import os
    rows_console, columns_console = os.popen('stty size', 'r').read().split()

    parser = argparse.ArgumentParser(
        description='Execute a stability analysis on a chemical network.'
                   +' Used principally for research in homochirality.',
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=40, width=int(columns_console)) )

    parser.add_argument('--model',
                        type=FileType('r'),
                        default='models/Kondepudi-Nelson_08.py',
                        help='Location of file holding chemical network model'
                            +' (default: models/Kondepudi-Nelson_08.py)',
                        metavar='file')
    parser.add_argument('--traditional',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable Traditional Analysis (default: true)',
                        metavar='(t|f)')
    parser.add_argument('--jacob-2',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable usage of a reduced jacobian matrix'
                            +' (2x2, left- and uppermost) for Traditional analysis (default: true)',
                        metavar='(t|f)')
    parser.add_argument('--belgian',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable Belgian-Serbian Analysis (default: true)',
                        metavar='(t|f)')
    parser.add_argument('--andrescarolina',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable Andres&Carolina Analysis (default: true).'
                            +' Caution: Activating this modifies the order of the reactions'
                            +' for all other analysis',
                        metavar='(t|f)')
    parser.add_argument('--andrescarolina2',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable Andres&Carolina 2 Analysis (default: true).',
                        metavar='(t|f)')
    parser.add_argument('--sna_with_sampling',
                        type=str2bool,
                        default=True,
                        help='Enable|Disable SNA + Andres&Carolina Criterion Analysis with sampling (default: true).',
                        metavar='(t|f)')
    parser.add_argument('--n_samples',
                        type=int,
                        default=1,
                        help='Number of samples to take (used only with --sna_with_sampling option) (default: 1).',
                        metavar='INT')
    parser.add_argument('--samples_folder',
                        type=str,
                        default=None,
                        help="Place to save samples (samples are save in chemulator's `simu.json' format) (used only with --sna_with_sampling option) (default: None).",
                        metavar='dir')
    args = parser.parse_args()
    #print(args)

    config = {
        'Traditional SNA Analysis': {
            'enable': args.traditional,
            'only 2 species for jacobian': args.jacob_2
        },
        'Belgian Analysis': {
            'enable': args.belgian
        },
        'Andres&Carolina Analysis': {
            'enable': args.andrescarolina
        },
        'Andres&Carolina Analysis 2': {
            'enable': args.andrescarolina2
        },
        'SNA + andrescarolina criterion': {
            'enable': args.sna_with_sampling,
            'n_samples': args.n_samples,
            'samples_folder': args.samples_folder
        }
    }

    main(config, args.model)
