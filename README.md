# How to run #

## Requirements ##

- Python 2, or Python 3
- NumPy
- SymPy
- Pyparsing
- Reduce Computer Algebra System (Necessary for one type of analysis, it isn't mandatory)

### In debian ###

```
apt-get install python-numpy python-sympy python-pyparsing gnuplot-x11
```

If you want to use python 3, change `python-numpy python-sympy python-pyparsing` for
`python3-numpy python3-sympy python3-pyparsing`

To be able to use `reduce` you need to install `gnuplot-x11`, and the prepackaged reduce
executable from [here][reduce].

[reduce]: https://sourceforge.net/projects/reduce-algebra/files/snapshot_2017-05-16/linux-deb/

We recomend to download the packages:

```
reduce-common_2017-05-16_all.deb
reduce-csl_2017-05-16_amd64.deb
reduce-addons_2017-05-16_amd64.deb
```

to install those packages run in the terminal:

```
dpkg -i reduce-common_2017-05-16_all.deb reduce-csl_2017-05-16_amd64.deb reduce-addons_2017-05-16_amd64.deb
```

## Running ##

Open the console and navigate (`cd`) to the folder where you have downloaded this
repository, and write:

```
$ python2 main.py --help
```

## LICENSE ##

This code is released under Apache 2.0

Copyright 2017 Universidad Nacional de Colombia
