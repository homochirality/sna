# Kondepudi-Nelson-Strecker: Model 1 - Production of AA - KNS-AP. Full species, does not work.
## It gets stuck in a loop in the calculations for V(J).
## Its Stoichiometric Matrix is singular, however the extreme currents are calculated.
modelname='KNS-AP: No cross inhibition. Production of AA. All species.'
species = ['L-CN', 'D-CN', 'L-AA', 'D-AA', 'HCN', 'INH', 'H2O', 'NH3']
reactions = [
   "HCN + INH <-> L-CN",
   "HCN + INH <-> D-CN",
   "L-CN + HCN + INH <-> 2 L-CN",
   "D-CN + HCN + INH <-> 2 D-CN",
    "L-CN + 2 H2O -> L-AA + NH3",
   "D-CN + 2 H2O -> D-AA + NH3"
]
