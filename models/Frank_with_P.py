# Frank . Belgian analysis doesn't work with P, stoichiometric matrix is square
modelname = 'Frank with P'
species = ['x', 'y', 'P']
reactions = [
    'x -> 2x',
    'y -> 2y',
    'x + y -> P'
]
