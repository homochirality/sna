# Kondepudi-Nelson # Belgian analysis does not work with this version of Kondepudi-Nelson because of the presence of external species.
modelname = "Kondepudi-Nelson: All species (without some reactions)"
species = ['L-X', 'D-X', 'A', 'B', 'P']
reactions = [
    "A + B <-> L-X",
    "A + B <-> D-X",
    "L-X + A + B <-> 2 L-X",
    "D-X + A + B <-> 2 D-X",
    "L-X + D-X -> P",
]
