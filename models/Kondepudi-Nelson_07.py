# Kondepudi-Nelson-Strecker: Model 1 - Production of AA. Forced to second order.
modelname='KNS-AP: No cross inhibition. Production of AA. Added P.'
species = ['L-CN', 'D-CN', 'P']
reactions = [
   " <-> L-CN",
   " <-> D-CN",
   "L-CN -> 2 L-CN",
   "D-CN -> 2 D-CN",
   "L-CN -> P", # Funciona, pero se debe analizar en detalle: ¿Qué implica el colocar P? ******
   "D-CN -> P " # ¿Vale la pena que el program admita reacciones duplicadas? ***************
   # Genera una matriz estequiométrica que sigue siendo de 8 reacciones. Sigue descartando las dos repetidas.
   # Tener dos reacciones que dan lo mismo implica sumar sus respectivas constantes de velocidad.
]
