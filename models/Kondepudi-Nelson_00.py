modelname = "Kondepudi-Nelson: All species"
species = ['CN-L', 'CN-D', 'INH', 'HCN', 'P']
reactions = [
    "INH + HCN <-> CN-L",
    "INH + HCN <-> CN-D",
    "CN-L + INH + HCN <-> 2 CN-L",
    "CN-D + INH + HCN <-> 2 CN-D",
    "CN-D + INH + HCN <-> CN-D + CN-L",
    "CN-L + INH + HCN <-> CN-L + CN-D",
    "CN-L + CN-D -> P",
]
