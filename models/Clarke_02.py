# Clarke model from reference Bruce Clarke 1988 Stoichiometric Network Analysis Cell Biophysics 12 237 - 253
# Protocolo 1 - Ruben. All species. As usual it does not work.
modelname='Clarke model 1988 - Example - All species minus te product B'
species = ['A', 'X', 'Y']
reactions = [
    'A -> X',
    'A -> Y',
    'Y <-> X',
    'X ->  ',
    'Y -> ',
]
