# Kondepudi-Nelson simplified-2
modelname = "Kondepudi-Nelson simplified - 2"
species = ['L-X', 'D-X']
reactions = [
    " -> L-X",
    " -> D-X",
    "L-X  -> 2 L-X",
    "D-X  -> 2 D-X",
    "L-X + D-X -> ",
]
