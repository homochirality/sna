modelname = "Kondepudi-Nelson original"
species = ['L-CN', 'D-CN', 'HCN', 'INH', 'ADD-CNL']
reactions = [
 'HCN + INH <-> L-CN',
 'HCN + INH <-> D-CN',
 'D-CN + HCN + INH <-> L-CN + D-CN',
 'L-CN + HCN + INH <-> L-CN + D-CN',
 '2 L-CN <-> L-CN + HCN + INH',
 '2 D-CN <-> D-CN + HCN + INH',
 'L-CN + D-CN -> ADD-CNL'
]
