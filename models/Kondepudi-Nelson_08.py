# KNS-AP-LES - Model 2 - Protocolo 3 #######
# Kondepudi-Nelson-Strecker-Amino acid Production Limited Enantio Selectivity - KNS-AP-LES: Model 2.
modelname='KNS-AP-LES: Model 2.'
species = ['L-CN', 'D-CN']
reactions = [
   " <-> L-CN",
   " <-> D-CN",
   "L-CN <-> 2 L-CN",
   "D-CN <-> 2 D-CN",
   "D-CN <-> L-CN + D-CN",
   "L-CN <-> D-CN + L-CN",
   "L-CN -> ", # Repeated reactions, reactions only count one time
   "D-CN -> "  #
]
