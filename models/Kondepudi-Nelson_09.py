# Kondepudi-Nelson-Strecker: Model 4 - KSIC-LES #
modelname='Model 4: KSIC-LES'
species = ['L-CN', 'D-CN']
reactions = [
 ' <-> L-CN',
 ' <-> D-CN',
 'L-CN <-> 2 L-CN',
 'D-CN <-> 2 D-CN',
 'L-CN <-> L-CN + D-CN',
 'D-CN <-> D-CN + L-CN',
 'L-CN + D-CN -> '
]
