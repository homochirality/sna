# Kondepudi-Nelson-Strecker: Model 1 - Production of AA - KNS-AP. Only isomer species.
modelname='KNS-AP: No cross inhibition. Production of AA. Only isomer species.'
species = ['L-CN', 'D-CN']
reactions = [
  " <-> L-CN",
  " <-> D-CN",
  "L-CN <-> 2 L-CN",
  "D-CN <-> 2 D-CN",
  "L-CN  -> ", # Ignores this two reactions given that
  "D-CN  -> "  # they're already in the list  ¡¡¡¡¡¡ Analizar esto con detalle !!!!!!
  # El efecto en la matriz es adicionar -1 0 y 0 -1 en las respectivas filas ¿Qué implica esto para la matriz?
]
