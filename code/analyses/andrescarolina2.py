# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import sympy as sym

from code.sna_analysis import eliminate_float_from_polynomial_with_whole_numbers

def run_analysis(rd):
    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape # number of species and number of reactions

    print("\n=== Analysis Andres-Carolina 2 ===")

    if(len(xs) < 3):
        print("Warning: Algorithm Andres-Carolina 2 requires a network with more than 2 species")
        print()
        return

    # 1.
    print("Differential equations:")
    eqs = S*R
    sym.pprint(eqs)
    print()

    # 2.
    jacob = eqs[:-1,:].jacobian( xs[:-1] )

    # 3. replace x2 for x1 (in andres notation i2 for i1), in equations and J
    print("Jacobian from eqs (excluding last eq, which should correspond to an equation that contains the product which doesn't make part of this system)")
    J = jacob.xreplace( {xs[1]:xs[0]} )
    sym.pprint(J)
    print()
    eqs = eqs.xreplace( {xs[1]:xs[0]} )

    # 4. create list of equations and inequations eq1 == 0 and eq2 == 0 and eq3 == 0 ... and det(J) != 0 and (J[0,0] - J[0,1] > 0)
    print("List of equations and inequalities that describe the the homochirality")

    print("- differential equations in a stationary state:")
    for e in eqs[:-1,0]:
        print(u"{} = 0".format(sym.pretty(e)))
    print()

    print("- jacobs determinant different to zero (det(J) != 0):")
    Jdet = J.det()
    print(u"{} != 0".format(sym.pretty(Jdet)))
    print()

    print("- andres&carolina inequality (J[0,0] - J[0,1] > 0):")
    print(u"{} > 0".format(sym.pretty(J[0,0] - J[0,1])))
    print()

    # 5. look if the set {(x,r) in reals^(n-1+r) : (x,r) fulfills the equations and inequations above}
    # take samples of the set

    # 6. sample the set (eqs == 0 and det(J)!=0) (i.e., no frank inequality), and calculate (# of samples that satisfy frank inequality / # of samples)
