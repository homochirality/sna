# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import sympy

from code.sna_analysis import eliminate_float_from_polynomial_with_whole_numbers

def run_analysis(rd, jacobWithOnly2Species=True):
    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape # number of species and number of reactions

    print("\n=== Jacobian without considering SNA or simmetry Andres-Carolina ===")
    print(" *** Classic stability analysis: Figure from book Gray-Scoot *** ")
    print()
    print("Jacobian matrix according to Scoot-Gray's book")

    if jacobWithOnly2Species:
        # taking only the first two equations corresponding to the isomers in andrescarolina notation
        diferential_system = (S*R)[:2,:]
        Jacob = diferential_system.jacobian( xs[:2] )
    else:
        Jacob = (S*R).jacobian( xs )

    # eliminanting annoying superflous floating numbers from Jacobian matrix
    Jacob = Jacob.applyfunc( eliminate_float_from_polynomial_with_whole_numbers )

    sympy.pprint(Jacob)
    print()
    print("Jacobian's trace")
    sympy.pprint(Jacob.trace().expand())
    print()
    print("Jacobian's determinant")
    sympy.pprint(Jacob.det().expand())
    print()

    # TODO: complete
    #if Jacob.det() < 0:
    #    print("*** You have saddle points ***")

    DiscriminantJ = Jacob.trace()**2 - 4*Jacob.det()
    print("Jacobian's discriminant")
    sympy.pprint(DiscriminantJ)
    print()
    discr_simplified = sympy.simplify(DiscriminantJ)
    if DiscriminantJ != discr_simplified:
        print("Jacobian's discriminant simplified")
        sympy.pprint(discr_simplified)
        print()

    print("Jacobian's eigenvalues and eigenvectors")

    eigenvals = Jacob.eigenvals()
    i=1
    for eigenvalue, geometric_mul, eigenvector in Jacob.eigenvects():
        print("Eigenvalue", i)
        print("(with algebraic multiplicity of:", eigenvals[eigenvalue], end=")\n")
        print("(and geometric multiplicity of:", geometric_mul, end=")\n")
        sympy.pprint(eigenvalue)
        print("Eigenvector of eigenvalue", i)
        sympy.pprint(eigenvector)
        i+=1

    print()
