# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

from code.sna_analysis import extreme_currents, get_ranges_for_inequalities, ranges_and_eqs_to_strs
import sympy
import numpy as np
import pprint
from code.reduce_cas import ReduceCASNotInstalledException
from sympy import oo
import sys
import json
from collections import OrderedDict

def run_analysis(rd, n_samples=1, samples_folder=None, modelname='model'):
    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape # number of species and number of reactions

    print("\n=== SNA Analysis + Andres&Carolina Criterion (and sampling) ===")
    print()

    duals_, not_duals = rd.get_duals()
    duals = {ks[s]:ks[f] for (f,s) in duals_}
    R_duals_replaced = R.xreplace( duals ).xreplace( {xs[1]:xs[0]} )

    if len(xs) < 2:
        print("Error, only one or none species given")
        print()
        return
    elif len(xs) > 2:
        print("Careful, last species will be removed in the process of creating jacobian for this reaction network. The jacobian is used to know if a sample state is hyperbolic or not")
        print()

    diff_eqs = S*R

    if len(xs) == 2:
        jacob = diff_eqs.jacobian( xs )
    else:
        jacob = diff_eqs[:-1,:].jacobian( xs[:-1] )

    jacob_duals_replaced = jacob.xreplace( duals ).xreplace( {xs[1]:xs[0]} )
    print("Jacobian from differential equations (ignoring last species, and replacing duals)")
    sympy.pprint(jacob_duals_replaced)
    print()

    #eigenvals = list(jacob_duals_replaced.eigenvals().keys())
    #print("Jacobian eigenvalues")
    #print(eigenvals)
    #print()

    if S.shape[0] != np.linalg.matrix_rank(S):
        print("Stoichiometric Matrix is SINGULAR!")
        print()

    E = extreme_currents(S)
    E = E.astype('int32')
    nE = E.shape[1] # number of extreme currents

    print("Extreme Currents Matrix")
    print(E, end="\n\n")

    J =  sympy.symbols( 'j0:%d' % nE ) # convex parameters
    E_omega_col = sum([(E[:,i]*J[i]).reshape( (nR,1) ) for i in range(nE)], sympy.zeros( nR,1 ))
    E_omega = sympy.diag(*E_omega_col)

    print("E_omega column")
    sympy.pprint(E_omega_col)
    print()

    VJ = S * E_omega * K.T

    print("V(J) Matrix")
    sympy.pprint(VJ)
    print()

    if any(e.is_zero for e in E_omega_col):
        for i in range(nR):
            if E_omega_col[i].is_zero:
                print("Warning: The reaction '{}' must be zero according to the ".format(rd.get_reactions()[i])
                     +"SNA analysis (the E_omega column is zero for that specific reaction (row))")
        print()
        #print("Warning: No further processing can be done with the model")
        #exit(1)

    ineq = VJ[0,0] - VJ[0,1]
    print("a - b > 0 criteria")
    print("{} > 0".format(ineq))
    print()

    eqs_js = [str(E_omega_col[f])+" = "+str(E_omega_col[s]) for (f,s) in duals_]
    print("Equations from duals pairs")
    print("[", end="")
    for e in eqs_js:
        print("{},".format(e))
        print(" ", end="")
    print("]\n")

    print("Calculanting valid values for js")
    # this uses redlog for calculations
    try:
        eqs_and_ranges = get_ranges_for_inequalities(
            [str(ineq)+">0"]
          + [str(j)+">0" for j in J]
          + eqs_js
    #      + ['j11 = 3']
    #    , verbose=True
        )
    except ReduceCASNotInstalledException as msg:
        print(msg, file=sys.stderr)
        return

    if eqs_and_ranges is None:
        print()
        print("No further calculations can be done!!")
        print("There are NOT any valid values for js")
        print("The equations are non-satisfiable")
        print()
        return

    # converting ranges and equalities into strings
    ranges = ranges_and_eqs_to_strs( eqs_and_ranges )

    print( "Values that j's can take to satisfy the a-b inequality (assuming j all positive)" )
    print("[", end="")
    for r in ranges:
        print("{}".format(r))
        print(" ", end="")
    print("]")
    print()

    if samples_folder:
        print("Sampling {} points and saving them in {}".format(n_samples, samples_folder))
        import os
        if not os.path.isdir(samples_folder):
            os.makedirs(samples_folder)

    # TODO: Use n_samples characteristic from sample_point
    for sample_i in range(1, n_samples+1):
        if samples_folder:
            print(".", end="")
            sys.stdout.flush()
        else:
            print("Sampling point {} of {}\n".format(sample_i, n_samples))

        sample_xs, sample_ks = \
                sample_point(rd, R_duals_replaced, J, eqs_and_ranges,
                             E_omega_col, n_samples=1, verbose=not samples_folder )
        hyperbolic = is_point_hyperbolic(rd, jacob_duals_replaced, duals, sample_xs, sample_ks,
                                         verbose=not samples_folder)

        if samples_folder:
            save_sample(rd, sample_i, sample_xs, sample_ks, hyperbolic, samples_folder, modelname)

    if samples_folder:
        print('\n')

    m_samples = 1000
    print("Calculating probability of frank state to occur (using {} sample points)".format(m_samples))
    try:
        eqs_and_ranges_not_only_frank = get_ranges_for_inequalities(
          [str(j)+">0" for j in J]
          + eqs_js
    #    , verbose=True
        )
    except ReduceCASNotInstalledException as msg:
        print(msg, file=sys.stderr)
        return
    print("  js restrictions without frank state calculated")

    sample_xs, sample_ks = \
            sample_point(rd, R_duals_replaced, J, eqs_and_ranges_not_only_frank,
                         E_omega_col, n_samples=m_samples, verbose=False )

    print("  {} points sampled".format(m_samples))
    ineq_frank = jacob_duals_replaced[0,0] - jacob_duals_replaced[0,1]

    total_frank = 0
    total_hyperbolic = 0
    for i in range(m_samples):
        sample_xs_i = { x:value[i] for x,value in sample_xs.items() }
        sample_ks_i = { x:value[i] for x,value in sample_ks.items() }
        frankvalue = ineq_frank.xreplace( sample_xs_i ).xreplace( sample_ks_i )
        if frankvalue > 0:
            total_frank += 1

        hyperbolic = is_point_hyperbolic(rd, jacob_duals_replaced, duals,
                                         sample_xs_i, sample_ks_i, verbose=False)
        if hyperbolic:
            total_hyperbolic += 1

    print("{} sampled points are frank".format(total_frank))
    print("%{} of sampled points are frank states".format(100.*total_frank/m_samples))
    print("{} sampled points are hyperbolic".format(total_hyperbolic))
    print("%{} of sampled points are hyperbolic".format(100.*total_hyperbolic/m_samples))

# TODO: accept an arbitrary number of samples
def is_point_hyperbolic(rd, jacob_duals_replaced, duals, sample_xs, sample_ks, verbose=False):
    S, R, xs = rd.S, rd.R, rd.xs
    duals_, not_duals = rd.get_duals()

    #eigenvals = list(jacob_duals_replaced.eigenvals().keys())
    #print(eigenvals)

    from numpy import linalg as LA

    sample_jacob = jacob_duals_replaced.xreplace( sample_ks ).xreplace( sample_xs )
    sj = sympy.matrix2numpy( sample_jacob ).astype( np.float64 )
    eigvals = LA.eigvals(sj)

    if verbose:
        print("Sampled jacobian")
        print(sj)
        print()

        print("Eigenvalues for sampled point")
        print(eigvals)
        print()

    hyperbolic = True
    for eigval in eigvals:
        if abs(eigval) < 1e-10: # smaller than 1e-10 means that the value is 0
            hyperbolic = False

    if verbose:
        print("Sampled point is{} hyperbolic".format('' if hyperbolic else ' NOT'))
        print()

    return hyperbolic

def sample_point(rd, R_duals_replaced, J, eqs_and_ranges, E_omega_col, n_samples=1, verbose=True ):
    #print("N-samples: {}".format(n_samples))

    xs, S = rd.xs, rd.S
    nS, nR = S.shape
    duals_, not_duals = rd.get_duals()

    # sampling js
    sample_js = [{} for _ in range(n_samples)]

    for (j, value_or_range) in eqs_and_ranges:
        # cheking if j is defined as a range
        if isinstance(value_or_range, sympy.Interval):
            start_ = [value_or_range.start.xreplace( s_js ) for s_js in sample_js]
            end_   = [value_or_range.end.xreplace( s_js ) for s_js in sample_js]
            #print(start_)
            #print(type(start_))
            assert all(s.is_Number for s in start_)
            start = np.array([float(s) for s in start_], dtype=np.float64)
            if end_[0] == oo:
                end = start + 100
            else:
                assert all(e.is_Number for e in end_)
                end = np.array([float(e) for e in end_], dtype=np.float64)

            value = np.random.uniform(start, end)
            #print("value {}: {}".format(j, value))
            #assert value_or_range.xreplace( sample_js[0] ).contains( value ), \
            #       "something went wrong, the range {} doesn't contain the sampled number {}".format(value_or_range, value)
            for i in range(n_samples):
                sample_js[i][j] = value[i]
        else: # it's a value
            for i in range(n_samples):
                sample_js[i][j] = value_or_range.xreplace( sample_js[i] )

    if verbose:
        print("  Random valid sample for j's")
        print("  [", end="")
        for j in J:
            # TODO: Reconstructing a arrays that were already calculated
            s_j = np.array([ sample_js[i][j] for i in range(n_samples) ], dtype=np.float64)
            print("{} = {}".format(j, s_j))
            print("   ", end="")
        print("]\n")

    R_values = [None for _ in range(n_samples)]
    for i in range(n_samples):
        R_values[i] = np.array( [E_omega_col.xreplace( sample_js[i] )], dtype='float').reshape( (nR,1) )

    sample_xs = {xs[0]: np.random.uniform(0, 100, size=n_samples)}
    for x in xs[2:]:
        sample_xs[x] = np.random.uniform(0,100, size=n_samples)

    if verbose:
        print("  xs samples")
        print("  [{} = {}\n   ".format(xs[0], sample_xs[xs[0]]), end="")
        for x in xs[2:]:
            print("{} = {}".format(x, sample_xs[x]))
            print("   ", end="")
        print("]\n")

    # NOTICE how these equations can be solved by hand now, all xs are independent
    # here, no need to apply the trick of the logarithm!
    R_xs_sampled = [ None for _ in range(n_samples) ]
    for i in range(n_samples):
        R_xs_sampled[i] = R_duals_replaced.xreplace( { x:s_x[i] for x,s_x in sample_xs.items() } )

    if verbose:
        print("  Equations to solve to get a ks sample")

    sample_ks = {}
    for i in [i for (i,j) in duals_] + not_duals:
        R_values_i_np = np.array([R_values[k][i,0] for k in range(n_samples)], dtype=np.float64)
        if verbose:
            print("  {} = {}".format( R_duals_replaced[i,0], R_values_i_np ))

        if isinstance(R_xs_sampled[0][i,0], sympy.mul.Mul):
            factor = np.zeros( n_samples )
            _, ki = R_xs_sampled[0][i,0].args
            for k in range(n_samples):
                factor[k], _ = R_xs_sampled[k][i,0].args
            sample_ks[ki] = R_values_i_np / factor
        elif isinstance(R_xs_sampled[0][i,0], sympy.symbol.Symbol):
            ki = R_xs_sampled[0][i,0]
            sample_ks[ki] = R_values_i_np

    if verbose:
        print()
        print("  ks samples")
        print("  [", end="")
        for ki, value in sample_ks.items():
            print("{} = {}".format(ki, value))
            print("   ", end="")
        print("]\n")

    return sample_xs, sample_ks

def save_sample(rd, sample_i, sample_xs, sample_ks, hyperbolic=None, samples_folder='samples', modelname='model'):
    sample_file = "{}/{}_{}".format(samples_folder, modelname, str(sample_i).rjust(3, '0'))
    if hyperbolic is not None:
        sample_file += "_{}hyperbolic".format('' if hyperbolic else 'no-')
    sample_file += '.simu.json'

    xs, ks, S = rd.xs, rd.ks, rd.S
    nS, nR = S.shape
    species, reactions = rd.species, rd.get_reactions()
    duals_, not_duals = rd.get_duals()

    content = { "ATOL": 1e-15, "Delta": 0.01, "RTOL": 1e-15, "T_0": 0, "Tmax": 200, "version": 0.1 }

    content['concentrations'] = OrderedDict()
    content['concentrations'][species[0]] = float( sample_xs[xs[0]] )
    content['concentrations'][species[1]] = float( sample_xs[xs[0]] )
    for i in range(2, nS):
        content['concentrations'][species[i]] = float( sample_xs[xs[i]] )

    content['reactions'] = OrderedDict()
    for i,j in duals_:
        content['reactions']['R{}'.format(i+1)] = reactions[i]
        content['reactions']['R{}'.format(j+1)] = reactions[j]
    for i in not_duals:
        content['reactions']['R{}'.format(i+1)] = reactions[i]

    content['rates'] = OrderedDict()
    for i,j in duals_:
        value_ki = sample_ks[ks[i]]
        content['rates']['R{}'.format(i+1)] = float( value_ki )
        content['rates']['R{}'.format(j+1)] = float( value_ki )
    for i in not_duals:
        content['rates']['R{}'.format(i+1)] = float( sample_ks[ks[i]] )

    with open(sample_file, 'w') as f:
        json.dump(content, f, indent=2)
