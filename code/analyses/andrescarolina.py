# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import sympy
import pprint
import sys # used for sys.stderr


from code.sna_analysis import eliminate_float_from_polynomial_with_whole_numbers, \
                              get_ranges_for_inequalities, ranges_and_eqs_to_strs
from code.reduce_cas import ReduceCASNotInstalledException

def xreplace_to_constant(obj, xs, c):
    return obj.xreplace( {x: c for x in xs} )

def obtain_andrescarolina_vars(reactions, S, ks):
    i = 0
    P = []
    for r in reactions['productive']:
        P.append(ks[i])
        i+=2
    D = []
    for r in reactions['destructive']:
        D.append(ks[i])
        i+=2
    A = []
    for r in reactions['autocatalytic']:
        A.append(ks[i])
        i+=2
    DE = []
    for r in reactions['decomposition']:
        DE.append(ks[i])
        i+=2
    E = []
    for r in reactions['enantio-selective']:
        E.append(ks[i])
        i+=2
    C = []
    for r in reactions['collision']:
        if len(r) == 2: # r is a dual pair
            Si = S[0,i] + S[1,i] # equal in paper to Ci1 + Ci2 - 2
            C.append( int(Si)*ks[i] )
            i+=2
        else: # r is self-dual
            Si = S[0,i] # equal in paper to Ci1 - 1
            C.append( int(Si)*ks[i] )
            i+=1
    return sum(P), sum(D), sum(A), sum(DE), sum(E), -sum(C)

def print_reactions_andrescarolina(reactions):
    print('{')
    for type, reactions in reactions.items():
        print("  '{}': [".format(type), end='')
        for reaction in reactions:
            print('\n      {},'.format(reaction), end='')
        print('],' if len(reactions)==0 else '\n  ],')
    print('}')

def run_analysis(rd, reactions_andrescarolina = None):
    if reactions_andrescarolina == None:
        reactions_andrescarolina = rd.get_reactions_andrescarolina()

    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape # number of species and number of reactions

    # detecting if reactions were all of the right type (productive, ...) and had dual pairs
    isAndresCarolina = len(reactions_andrescarolina['others']) == 0

    print(" *** Reactions grouped by Andres & Carolina types: *** ")
    print_reactions_andrescarolina(reactions_andrescarolina)
    print()

    if not isAndresCarolina:
        print("*** Failure for Andres & Carolina algorithm ***")
        print("Some reactions don't have a dual pair or aren't of one of the known types:")
        print("productive, destructive, autocatalytic, decomposition, enantio-selective, and collision")
        print()
    else:
        print(" *** Andres & Carolina analysis *** ")
        print()

        # getting variables defined in Andrés & Carolina paper
        P, D, A, DE, E, C = obtain_andrescarolina_vars(reactions_andrescarolina, S, ks)

        if len(reactions_andrescarolina['autocatalytic']) == 0:
            print("The network does not admit Frank states !!!")
            print("There are no autocatalytic reactions in the model, and they are required for the network to admit Frank states")
            print()

        # Detecting if there aren't any negative stoichiometric coefficients from the collision pairs
        # Note that we are checking here for the contrary condition, that there should be at least one positive
        # coefficient, this is due C is defined as -sum(stoichiometric * rate)
        elif C == 0 or all( [ coeff < 0 for (ks_, coeff) in C.as_poly().terms() ] ):
            print("The network does not admit Frank states !!!")
            print("There needs to be at least one negative stoichiometric coefficient for a collision reaction so that the network admits Frank states")
            print()

        else:
            diferential_system = (S*R)[:2,:] # taking only the first two equations corresponding to the isomers
            Jacob = diferential_system.jacobian( xs[:2] )

            ## eliminanting annoying superflous floating numbers from Jacobian matrix
            Jacob = Jacob.applyfunc( eliminate_float_from_polynomial_with_whole_numbers )

            # getting number of self-dual reactions in collision group reactions
            self_duals_collision = len( [None for r in reactions_andrescarolina['collision'] if len(r)==1] )
            assert self_duals_collision<=1, "Number of self-dual pairs (of collision type) is bigger than 1, this is not yet supported"
            # replacing all variables that are meant to be equal
            ks_for_andrescarolina = {
                ks[2*i+1] : ks[2*i]
                for i in range(nR//2) # TODO: this isn't considering the case there is more than one self-dual pair (look assert up)
            }
            Jacob = Jacob.xreplace( {xs[1]: xs[0]} ) # replacing all x1 appearances with x0
            Jacob = Jacob.xreplace( ks_for_andrescarolina )

            print("Jacobian matrix (with dual pairs reaction rates equaled):")
            sympy.pprint(Jacob)
            # sympy.pprint(Jacob.applyfunc( eliminate_float_from_polynomial_with_whole_numbers ))

            assert Jacob[0,0] == Jacob[1,1] and Jacob[0,1] == Jacob[1,0] \
                    , "Jacobian matrix should be symetric of the form [[a,b],[b,a]]"

            a = Jacob[0,0]
            b = Jacob[0,1]

            print()
            print("a =", a)
            print("b =", b)

            ineq1 = xreplace_to_constant(   a - b > 0   , xs , 1 )
            ineq2 = xreplace_to_constant(   a + b < 0   , xs , 1 )

            print()
            print("Inequalities (assuming concentrations equal to 1)")
            print("a - b > 0   =>  ", ineq1 )
            print("a + b < 0   =>  ", ineq2 )

            # testing the use of variables also works: P, E, D, DE, A, C to find inequalities
            assert ineq1 == (A - (D + 2*DE + E) > 0) \
               and ineq2 == (A + E - (D + 2*DE + 2*C) < 0) \
                    , "Jacobian method and Andrés&Carolina methods should yield the same"

            # creating equality as defined in paper
            eq = D + DE + C - E - A - P

            print("Equality:")
            print(eq, "= 0")
            print()

            # this uses redlog for calculations
            try:
                ranges_and_eqs = get_ranges_for_inequalities(
                    [str(ineq1), str(ineq2), str(eq)+"=0"]
                  + [str(k)+">0" for k in ks_for_andrescarolina.values()]
                )
            except ReduceCASNotInstalledException as msg:
                print(msg, file=sys.stderr)
                exit(1)

            # converting ranges and equalities into strings
            ranges = ranges_and_eqs_to_strs( ranges_and_eqs )

            print( "Values that k's can take to satisfy inequalities and equality" )
            pprint.pprint( ranges )
            print()
