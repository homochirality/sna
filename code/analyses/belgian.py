# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import numpy as np
import sympy
import pprint
import os
from functools import reduce
rows_console, columns_console = os.popen('stty size', 'r').read().split()

from code.sna_analysis import eliminate_float_from_polynomial_with_whole_numbers, extreme_currents

from itertools import combinations
def mineurs(VJ, limit=0):
    sizeVJ = VJ.shape[0]
    if limit<1:
        limit = sizeVJ
    mineurs = []
    # searching in determinants of principal matrices for negative terms
    for n in range(1, min(limit, sizeVJ)+1):
        for index in combinations( range(sizeVJ), n ):
            mineur_ = VJ[index, index].det().expand()
            if mineur_ == 0:
                continue # don't even waste your time trying to find any negative coefficients on this mineur
            mineur = mineur_.as_poly()
            # print(mineur)
            negative_coefficients = { gens: coeff for gens, coeff in mineur.terms() if coeff < 0}
            if len(negative_coefficients) > 0: # there is at least a term with a negative coefficient
                # showResults(mineur, index, -1 * negative_coefficients, save_to_file)
                mineurs.append({
                    'mineur': mineur,
                    #'negative coefficients': negative_coefficients,
                    'index': index
                })
    return mineurs

import operator
def mul(xs, unity=1):
    return reduce(operator.mul, xs, unity)

def mineur_to_inequality(mineur):
    """Recives a sympy polynomial and creates a unequality from it"""
    nGens = len(mineur.gens)
    negative, positive = [], [] # negative and positive expressions of mineur (a principal determinant of VJ)
    for exps, coeff in mineur.terms():
        # mineur.gens contains all Js, e.g., (J1, J2, J3)
        # exps contains the exponents for each Jn
        term = abs(coeff) * mul([ mineur.gens[j]**exps[j] for j in range(nGens) ])
        (negative if coeff < 0 else positive).append(term) # if term is negative, then add it to negative terms, otherwise to positive
    return sum(negative) - sum(positive) > 0

def run_analysis(rd):
    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape # number of species and number of reactions

    print(" *** Belgian-based SNA analysis *** ")
    print()

    failed_E = False
    try:
        E = extreme_currents(S)
    except Exception as msg:
        print("*******")
        print(msg)
        print("*******")
        failed_E = True

    if not failed_E:
        E = E.astype('int32')
        nE = E.shape[1] # number of extreme currents

        print("Extreme Currents Matrix")
        print(E, end="\n\n")

        J =  sympy.symbols( 'j0:%d' % nE ) # convex parameters
        # E_omega = sum([sympy.diag(*E.T[i])*J[i] for i in range(nE)], sympy.zeros( nR, nR ))
        E_omega_prim = sum([E[:,i].reshape(nR,1)*J[i] for i in range(nE)], sympy.zeros( nR, 1 ))

        print("E_omega_prim matrix")
        sympy.pprint(E_omega_prim)
        print()

        # VJ_ = S * E_omega * K.T
        VJ_ = S * sympy.diag(*E_omega_prim) * K.T
        VJ = VJ_.applyfunc( eliminate_float_from_polynomial_with_whole_numbers )

        print("V(J) Matrix")
        sympy.pprint(VJ)
        print()

        print("\n==== Mineur Analysis ====")

        limit = 6 # limit to search for mineurs
        print("Limiting mineur analysis to mineurs of size smaller or equal to", limit)
        print()

        # TODO: Check if VJ must be multiplied by -1 or it shouldn't
        ms = mineurs(-VJ, limit)

        if len(ms) == 0:
            print("There are no negative terms in the determinant of any mineur of size <=", limit)
            #exit(0)

        for mineur in ms:
            print("Mineur:")
            sympy.pprint(mineur["mineur"])
            #print("Negative coefficients in mineur:")
            #sympy.pprint(mineur["negative coefficients"])
            print("Compounds number:", mineur["index"])
            print()

        serbs_inequalities = ["j{} > 0".format(i) for i in range(nE)] + [str(mineur_to_inequality(mineur["mineur"])) for mineur in ms]
        print("Serb's inequalities")
        pprint.pprint(serbs_inequalities, width=columns_console)

        print("\n==== Matrix V(J) ====")
        print(" *** Classic stability analysis: Figure from book Gray-Scoot *** ")
        print()

        print("Trace of the V(J) matrix")
        sympy.pprint(VJ.trace().expand())
        print()
        print("Determinant of the V(J) matrix")
        sympy.pprint(VJ.det().expand())
        print()

        #if VJ.det() < 0:
        #    print("*** You have saddle points ***")

        Discriminant = VJ.trace()**2 - 4*VJ.det()
        print("Discriminant")
        sympy.pprint(Discriminant)
        print()

        #if Discriminant > 0:
        #    print("Only real roots: Node attractors or saddle points.")

        print(" ** Eigenvalues and eigenvectors of the VJ matrix ** ")
        eigenvals = VJ.eigenvals()
        i=1
        for eigenvalue, geometric_mul, eigenvector in VJ.eigenvects():
            print("Eigenvalue", i)
            print("(with algebraic multiplicity of:", eigenvals[eigenvalue], end=")\n")
            print("(and geometric multiplicity of:", geometric_mul, end=")\n")
            sympy.pprint(eigenvalue)
            print("Eigenvector of eigenvalue", i)
            sympy.pprint(eigenvector)
            i+=1
        print()
