# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from subprocess import Popen, PIPE, STDOUT
import textwrap, re
import sympy

class ReduceCASErrorSignalException(Exception):
    def __init__(self, message):
        super(ReduceCASErrorSignalException, self).__init__(message)
        self.message = message

class ReduceCASNotInstalledException(Exception):
    def __init__(self, message):
        super(ReduceCASNotInstalledException, self).__init__(message)
        self.message = message

class ReduceCAS(object):
    def __init__(self, verbose=False, extra_args=[]):
        self.verbose = verbose
        reduce = self.__find_reduce_exec() # getting of installed reduce
        program = [reduce, '--nogui', '-q'] + extra_args
        if self.verbose:
            print("ReduceCAS debug: starting '{}'".format(program))
        self.p = Popen(program, stdout=PIPE, stdin=PIPE, stderr=STDOUT, bufsize=0)
        self.p.stdin.write( b'off nat;\n' )
        for _ in range(3):
            self.p.stdout.readline()

        self.__ineq_module_started = False

    def __find_reduce_exec(self):
        import distutils.spawn

        if distutils.spawn.find_executable('reduce'):
            return 'reduce'
        elif distutils.spawn.find_executable('redcsl'):
            return 'redcsl'
        else:
            raise ReduceCASNotInstalledException(
                "No `reduce` executable found!\n"+
                "Please install `reduce` and make it accessible in the PATH variable\n"+
                "with the name `reduce` or `redcsl`. See README.md for more details."
            )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.finish_session()
        return False

    def exec_command_raw( self, cmd ):
        if cmd.count(';') > 1:
            raise Exception('only one comand to be executed by `exec_command_raw`, multiple ; in string found')
        wrapped_cmd = '\n'.join(textwrap.wrap(cmd, 70))
        self.p.stdin.write( str.encode(wrapped_cmd) )
        self.p.stdin.write( b'\n' )
        out_line = self.p.stdout.readline() # this should have a line starting by a number and followed by a column
        if self.verbose:
            print("ReduceCAS debug: output line (trash) {}".format(repr(out_line)))
        end_output_line = False
        lines = []
        while not end_output_line:
            fragment = self.p.stdout.readline().decode()
            if self.verbose:
                print("ReduceCAS debug: output line {}".format(repr(fragment)))
            lines.append( fragment.strip('\n') )
            if fragment == '':
                raise ReduceCASErrorSignalException('Reduce closed inexpectedly, please report this to upstream')
            trailing = fragment.rfind( '$' )
            if trailing >= 0:
                end_output_line = True
                lines[-1] = lines[-1][:trailing] # removing trailing $ symbol from last fragment
                self.p.stdout.readline()
            if fragment.rfind( '+++' ) == 0:
                raise ReduceCASErrorSignalException('Reduce sent an unexpected error, please report this.\n'
                                                   +'Error reported: "{}"'.format(fragment[:-1]))
        return ''.join(lines)

    def exec_command_raw_no_return( self, cmd ):
        if cmd.count(';') > 1:
            raise Exception('only one comand to be executed by `exec_command_raw`, multiple ; in string found')
        wrapped_cmd = '\n'.join(textwrap.wrap(cmd, 70))
        if self.verbose:
            print("ReduceCAS debug: executing command '{}'".format(wrapped_cmd))
        self.p.stdin.write( str.encode(wrapped_cmd) )
        self.p.stdin.write( b'\n' )
        self.p.stdout.readline() # this should have a line starting by a number and followed by a column

    def finish_session(self):
        self.p.stdin.close()
        # self.p.stdout.close()
        self.p.wait()

    def force_finish(self):
        self.p.kill()

    def eliminate_quantifiers(self, formula):
        assert formula.find(';') < 0 and formula.find("$") < 0 # a formula shouldn't contain any ; or $ symbols
        # TODO: define more asserts or return failure

        self.exec_command_raw( 'rlset ofsf;' )
        self.exec_command_raw( "phi := {};".format(formula) )
        return self.exec_command_raw( 'rlqe phi;' )

    def get_ranges_for_inequalities(self, ineqs_list):
        '''
        "Solves" a system of lineal equations. Returns a list with the values each
        variable must take to satisfy the lineal equations. Returns None if the
        system has no solution.
        '''
        if not self.__ineq_module_started:
            self.exec_command_raw_no_return( 'load ineq;' )
            self.__ineq_module_started = True
        #list_str = [sympy.pretty(i, use_unicode=False) for i in ineqs_list]
        ineqs_str = ' , '.join(ineqs_list)
        ineqs_str = re.sub('>([^=])', r">=\1", ineqs_str) # replacing all appearances of `>` with `>=`
        ineqs_str = re.sub('<([^=])', r"<=\1", ineqs_str) # replacing all appearances of `<` with `<=`
        command = 'ineq_solve({{ {} }});'.format( ineqs_str )
        if self.verbose:
            print("ReduceCAS debug: executing command '{}'".format(command))
        result = self.exec_command_raw( command )
        if self.verbose:
            print("ReduceCAS debug: result from reduce '{}'".format(result))

        ranges_result = self.__parse_ineq_result( result )

        # Detecting if the system is non-satisfiable even though Reduce doesn't mentions it
        satisfiable = True
        if not isinstance(ranges_result[0][1], sympy.Interval):
            variables_values = {}
            i = 0
            while not isinstance(ranges_result[i][1], sympy.Interval) and i < len(ranges_result):
                (var, value) = ranges_result[i]
                variables_values[var] = value.xreplace(variables_values)
                i += 1
            while i < len(ranges_result):
                (var, value) = ranges_result[i]
                if value.xreplace( variables_values ).is_EmptySet:
                    satisfiable = False
                    if self.verbose:
                        print("ReduceCAS debug: result from reduce is non-satisfiable")
                        print("ReduceCAS debug: {} can't take any value given {}".format(var, variables_values))
                    break
                i += 1

        return ranges_result if satisfiable else None

    def __create_sympy_output_parser(self):
        from pyparsing import alphas, alphanums, Word, delimitedList, Suppress, \
                              nestedExpr, Keyword, Literal, oneOf, nums, Forward, \
                              infixNotation, opAssoc, ZeroOrMore, Group, Suppress, \
                              ParseException

        varname = Word(alphas+'_', alphanums+'_').setParseAction(lambda t: t[0])
        infinity = Keyword('infinity').setParseAction(lambda t: "oo")
        LPAR,RPAR,COMMA = map(Suppress, "(),")
        number = Word(nums)
        min_fun = Forward()
        max_fun = Forward()
        variadic_fun_content = Forward()

        def operationAction(t):
            return "("+"".join(t[0])+")"

        simple_expr = infixNotation(infinity | min_fun | max_fun | varname | number,
                    [ (Literal('-'), 1, opAssoc.RIGHT, operationAction),
                      (oneOf('* /'), 2, opAssoc.LEFT,  operationAction),
                      (oneOf('+ -'), 2, opAssoc.LEFT,  operationAction)])

        variadic_fun_content << simple_expr + ZeroOrMore( COMMA + variadic_fun_content )
        min_fun << (Keyword('min') + LPAR + variadic_fun_content + RPAR)('min').setParseAction(lambda t: "Min("+",".join(t[1:])+")")
        max_fun << (Keyword('max') + LPAR + variadic_fun_content + RPAR)('max').setParseAction(lambda t: "Max("+",".join(t[1:])+")")

        a_range = (LPAR + simple_expr("start") + Literal("! !.!.!").suppress() + simple_expr("end") + RPAR).setParseAction(lambda t: "Interval.open("+t[0]+","+t[1]+")")
        varassign = Group( varname("id") + Suppress("=") + (simple_expr("equation") | a_range("range")) )

        results_parser = delimitedList( varassign )

        return results_parser

    # example use:
    # __parse_ineq_result('{j6=(0! !.!.! infinity),j7=(0! !.!.! infinity),'
    #                     'j4=(0! !.!.! j6 + j7),j3=(max(0,j4 - j6)! !.!.! infinity),'
    #                     'j1=(max(0, - j4 + j6)! !.!.! infinity),j5= - j4 + j6 + j7,'
    #                     'j2=j3 - j4 + j6,j0=j1 - j2 + j3}'
    #                    )

    def __parse_ineq_result(self, result):
        """
        Converts the string resulting of calling reduce's command `ineq_solve`
        into a dictionary of lists, with the next components:
            (
                `list of (symbol, sympy interval)`,
                `list of (symbol, sympy polynomial)`
            )
        """
        from pyparsing import ParseException

        def raise_parsing_exception():
            raise Exception("__parse_ineq_result cannot parse result from `ineq_solve` (below) as list of assignation possibilities.\n"
                            "Result from `ineq_solve`:\n{}"
                            .format(result))

        parser = self.__create_sympy_output_parser()

        if result == '{}':
            return None
        else:
            multiple_results_match = re.match('{(.*)}', result) # checking if result is enclosed in brackets `{}`
            try:
                results_from_parser = \
                    parser.parseString( multiple_results_match.group(1) if multiple_results_match else result, parseAll=True )
            except ParseException as msg:
                #print(msg, file=sys.stderr)
                raise Exception("Reduce output couldn't be parsed, I don't know how to interpret '{}'".format(result))

            if self.verbose:
                print("ReduceCAS debug: parsed results from reduce output {}".format(results_from_parser))

            ranges_started = False
            equalities_and_ranges = []
            for i in range(len(results_from_parser)):
                result_i = results_from_parser[i]
                if 'range' in result_i:
                    equalities_and_ranges.append( (sympy.symbols(result_i.id), sympy.sympify(result_i.range)) )
                else: # 'equation' in result_i
                    equalities_and_ranges.append( (sympy.symbols(result_i.id), sympy.sympify(result_i.equation)) )

            return equalities_and_ranges
