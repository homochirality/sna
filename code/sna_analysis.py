# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division
import sys
import numpy as np

## Type imports for type checking with MYPY
#if 'typing' in sys.modules.keys():
#    from typing import (
#        List,
#    )

from functools import reduce
#from sympy.parsing.sympy_parser import parse_expr
import code.reduce_cas as redcas

def extreme_currents(SC):
    Tiny = 1e-10
    Nr = SC.shape[1] # number of reactions
    Ra = np.linalg.matrix_rank( SC )

    #SC_singular = False
    # Checking if stoichiometric matrix is singular
    # if it is, some repeated rows are eliminated from the matrix
    if Ra < SC.shape[0]:
        #SC_singular = True
        SC = SC[ __columns_to_keep(SC) ]

    if Ra == SC.shape[1]: # SC is a non-singular square matrix
        raise Exception("Stoichiometric Matrix is square, therefore there are no extreme currents for this system")

    N1 = Nr-SC.shape[0]-1

    B = np.zeros( (Nr, 1) )
    B[-1] = 1
    Nsol = 0
    E = []
    # searching for all extreme currents
    for N1_ones_array in __unique_permutations( [0]*(Nr-N1) + [1]*N1 ):
        # k = reduce( lambda acc, bit: 2*acc+bit, N1_ones_array, 0 ) # number represented by N1_ones_array
        indeces = [i for (i, num) in enumerate(N1_ones_array) if num == 1]

        A1 = np.zeros( (N1, Nr) )
        for i in range(N1):
            A1[i, indeces[i]] = 1

        found = False
        A2 = np.zeros( (1,Nr) )
        for colA in range(Nr):
            A2[0,colA] = 1
            A = np.concatenate( (SC,A1,A2), axis=0 )
            if not found and np.linalg.matrix_rank(A)==Nr:
                Ej = np.linalg.solve(A, B)
                #print(A)
                #print()
                found = True
                if np.amin(Ej) > -Tiny:
                    #print(A)
                    #print()
                    to_add = True
                    for colE in range(Nsol):
                        if ( Ej - E[colE] < Tiny*np.ones( (Nr,1) ) ).all():
                            to_add = False # solution already in the list of solutions
                    if to_add:
                        E.append( Ej )
                        # print("K =", k, " \tSolution #", Nsol+1)
                        Nsol += 1

    # Scaling each extreme current to integer numbers
    for i in range(Nsol-1):
        # first, find the smallest number in each column of E that is not zero (bigger than Tiny)
        # then, divide each column by the smallest number found
        E[i] /= min( [Eij for Eij in E[i].T.tolist()[0] if Eij > Tiny] )
        # TODO: dividing by the smallest doesn't necessary gives all numbers as reals
        # possible better approach, divide by all numbers smaller than one, and divide by gcd of them all

    # creating np.array from list of E[i] np.arrays (of sizes (1,Nr))
    #return {
    #    'E': np.concatenate(E, axis=1).round(),
    #    'SC_singular': SC_singular
    #}
    null_array = np.array([]).reshape( (Nr, 0) )
    return np.concatenate(E + [null_array], axis=1).round()

## From https://stackoverflow.com/a/30558049
def __unique_permutations(elements):
    if len(elements) == 1:
        yield (elements[0],)
    else:
        unique_elements = set(elements)
        for first_element in unique_elements:
            remaining_elements = list(elements)
            remaining_elements.remove(first_element)
            for sub_permutation in __unique_permutations(remaining_elements):
                yield (first_element,) + sub_permutation

def __columns_to_keep(A):
    """
    Using gaussian elimination to know which columns to keep to make Ran(A) == A.shape[0],
    ie. which columns to preserve to make the matrix A non-singular
    A is a np.array
    """
    n, m = A.shape
    A = A.copy()
    indeces = np.array(range(n))

    l = -1
    k = 0
    while k < n and l < m-1:
        l += 1
        maxindex = abs(A[k:, l]).argmax() + k
        if A[maxindex, l] == 0:
            continue
        #Swap rows
        if maxindex != k:
            A[[k,maxindex]] = A[[maxindex, k]]
            indeces[[k,maxindex]] = indeces[[maxindex, k]]
        for row in range(k+1, n):
            multiplier = A[row,l]/A[k,l]
            # the only one in this column since the rest are zero
            for col in range(l, m):
                A[row,col] -= multiplier*A[k,col]

        k += 1

    #print(k, l)

    if l==m-1:
        toRet = indeces[:k]
        toRet.sort()
        return toRet #, A, indeces
    else:
        return np.array([])#, A, indeces

import operator
from sympy.core.power import Pow
def poly_to_list_with_sign(pol):
    if pol == 0:
        return []
    pol_ = pol.as_poly().terms()
    vars = list(pol.as_poly().gens)
    mult = lambda xs: reduce(operator.mul, xs, 1)
    for i in range(len(vars)): # eliminating powers with floating exponents from all terms
        if isinstance( vars[i], Pow ):
            vars[i] = vars[i].base ** int(vars[i].exp)
    return [ (
                -1 if p[1]<0 else 1, # sign of term
                abs(int(p[1])) * mult( v**t for v,t in zip(vars, p[0]) ) # a term of the polynomial with its coefficient
             )
             for p in pol_
           ]

def eliminate_float_from_polynomial_with_whole_numbers(pol):
    return sum( s*t for (s,t) in poly_to_list_with_sign(pol) )

### CODE DEPRECATED, the new criteria is: a - b > 0 and a + b < 0 ###
#def separate_neg_and_pos_form_poly(pol):
#    pol_ = poly_to_list_with_sign(pol)
#    pos = sum( [t for (s, t) in pol_ if s > 0] )
#    neg = sum( [t for (s, t) in pol_ if s < 0] )
#    return pos, neg
#
#def apply_criteria_for_instability(VJ, E_omega_prim, nR, nS, dual_pairs, nE):
#    assert VJ.shape == (2,2), "Only Jacobian matrices can be analysed for now"
#    assert nS == 2, "Only systems with two species can be analysed now, " \
#                    "therefore no jacobian of shape != (2,2) should be accepted either"
#    # when all js in VJ are evaluated to a number, the resultant VJ (Jacobian) is of the form
#    # a  b    VJ[0,0] VJ[0,1]
#    # b  a    VJ[1,0] VJ[1,1]
#    # The idea is to find solutions for the characteristic polynomial that are
#    # positive, if there are, it exists the possibility that the system will be
#    # unstable on those values for which the solutions (for the characteristic
#    # polynomial) are positive
#    # The characteristic polynomial of VJ is:
#    # det( a - λ    b   ) = (a-λ)**2 + b**2
#    #    (   b    a - λ )
#    # and those the solutions for λ are:
#    # λ = a+b , and
#    # λ = a-b
#    # TODO: add reference to (to be published) paper explaining the algorithm
#
#    #####  creating help variables  #####
#    # Creating the Velocity
#    # Function vector ignoring the concentration of each species
#    # (this only makes sense since the two concentrations must coincide and each pair on
#    # dual_pairs should make every pair of velocity functions (f1(x0,x1), f2(x0,x1))
#    # differenciate only on the terms x0 and x1, replacing x1 for x0 and x0 for x1 in f1
#    # should result in f2 (look at R and dual_pairs to know what I'm talking about))
#    R_ignoring_xs = sympy.Matrix( sympy.symbols('k0:{}'.format(nR)) )
#    # VJ_ignoring_xs = S * sympy.diag(*R_ignoring_xs) * K.T
#
#    # all systems should fulfill these inequalities
#    general_inequalities = (
#        ['j{} > 0'.format(i) for i in range(nE)]
#      + ['k{} > 0'.format(i) for i in range(nR) if not any( e==i for s,e in dual_pairs )]
#      + ['x0 > 0', 'x1 > 0'] # ['x{} > 0'.format(i) for i in nS]
#      + ['x0 = x1']
#      # + ['k{} = k{}'.format(i, j) for i,j in dual_pairs]
#      + ['{} = {}'.format(VJ[0,0], VJ[1,1]), '{} = {}'.format(VJ[0,1], VJ[1,0])] # a == a and b == b # VJ[0,0] == VJ[1,1] and  VJ[0,1] == VJ[1,0]
#      # + ['{} = {}'.format(k, js) for (k, js) in zip(R_ignoring_xs, E_omega_prim)]
#      + ['{} = {}'.format(k, js) for (k, js) in zip(R, E_omega_prim)]
#    )
#
#    ##### Beginning #####
#    a_pos, a_neg = separate_neg_and_pos_form_poly(VJ[0,0])
#    if a_pos != 0: # i.e., a > 0, it is certain that there will be a negative solution for the characteristic polynomial
#        inequality = a_pos - a_neg > 0
#        # TODO: create inequality for V[1,1], it may help on reducing the number of possibilities
#        # if ...: # now try with b > 0 and b < 0
#        pass
#    else: # a < 0
#        # trying with: b < a , if this is true then: λ = a-b is positive, bingo!! one solution is positive
#        inequality_a_lt_0__b_lt_a = '{} + {} > 0'.format(VJ[0,0], -VJ[0,1])
#        formula_for_a_lt_0__b_lt_a = replace_dual_pairs( dual_pairs,
#            ''.join( ['ex(j{}, '.format(i) for i in range(nE)] )
#          + ' and '.join(general_inequalities)
#          + ' and {}'.format(inequality_a_lt_0__b_lt_a)
#          + ' )'*nE
#        )
#
#        inequalities = eliminate_quantifiers( formula_for_a_lt_0__b_lt_a )
#        # trying with: b > -a , if this is true then: λ = a+b is positive, bingo!! one solution is positive
#        # nothing worked :/, that means both solutions λ are negative, therefore the system is stable
#
#import re
#def replace_dual_pairs(dual_pairs, formula):
#    rep = { re.escape(k): v
#            for k,v
#            in [('k{}'.format(j), 'k{}'.format(i)) for i,j in dual_pairs]
#          }
#    pattern = re.compile("|".join(rep.keys()))
#    return pattern.sub(lambda m: rep[re.escape(m.group(0))], formula)


def simplify_inequalities(ineqs_list):
    # type: (List[str]) -> List[str]
    from subprocess import Popen, PIPE, STDOUT
    import sympy, re, textwrap

    ineqs_as_str = ' and '.join(ineq for ineq in ineqs_list)
    ineqs_as_str = ineqs_as_str.replace('.0', '') # eliminating decimal point
    ineqs_as_bytes = '\n'.join(textwrap.wrap(ineqs_as_str, 70)).encode() # breaking string to pieces of at most 70 characters
    print(ineqs_as_bytes)

    slfq = Popen(['./extern/slfq', '-q', '-F'], stdout=PIPE, stdin=PIPE, stderr=STDOUT, bufsize=0)
    (stdoutdata, _) = slfq.communicate( ineqs_as_bytes )

    if slfq.returncode != 0: # it exited annormaly :S
        raise Exception("slfq cannot simplify: {}\nOutput error from slfq:\n{}".format(ineqs_as_str, stdoutdata))

    # converting slfq to sympy format
    stdoutdata = stdoutdata.decode().strip()
    to_replace = {
        '['   : '(',
        ']'   : ')',
        '/\\' : 'and',
        '\\/' : 'or',
        '^'   : '**',
        '/='  : '!=',
        '\n'  : '',
    }
    for from_, to_ in to_replace.items():
        stdoutdata = stdoutdata.replace(from_, to_)

    if '('==stdoutdata[0]: # if formula has '()' symbols at the beginnig and end delete them
        stdoutdata = stdoutdata[1:-1]

    ineqs_list_to_ret = []
    for ineq in [atom.strip() for atom in stdoutdata.split('and')]:
        # Putting multiplication symbol `*` between variables
        if re.search( '(\w)\s+(\w)', ineq ):
            new_ineq = re.sub( '(\w)\s+(\w)', lambda matchobj: matchobj.group(1)+"*"+matchobj.group(2), ineq)
        else:
            new_ineq = ineq
        ineqs_list_to_ret.append( new_ineq )

    # TODO: add an assert to hoping that expr from stdoutdata is a conjuction (only 'and's)

    return ineqs_list_to_ret

def formula_with_quantifiers(num_species, num_reaction_speeds, dual_pairs, E_omega, R):
    atoms = ['x0 = x1']
    atoms.extend( 'x%d > 0' % i for i in range(num_species) )
    atoms.extend( 'k%d > 0' % i for i in range(num_reaction_speeds) )
    atoms.extend( 'k%d = k%d' % (a, b) for (a, b) in dual_pairs )
    atoms.extend( '%s = %s' % (str(E_omega[i,i]), str(R[i])) for i in range(num_reaction_speeds) )
    quantifiers_begining = []
    quantifiers_begining.extend( 'ex(x%d, ' % i for i in range(num_species) )
    quantifiers_begining.extend( 'ex(k%d, ' % i for i in range(num_reaction_speeds) )
    quantifiers_ending = ')'*(num_species+num_reaction_speeds)
    return ''.join(quantifiers_begining) + ' and '.join(atoms) + ' ' + quantifiers_ending

def eliminate_quantifiers(formula, verbose=False):
    """
    Using `reduce` computational algebra system to eliminate quantifiers from first order forumla
    """
    no_quantifiers = None
    with redcas.ReduceCAS(verbose=verbose) as red:
        no_quantifiers = red.eliminate_quantifiers(formula)
    return no_quantifiers

def formula_to_inequalities_list( string_formula ):
    """
    Takes a  string of the form "`ineq` and `ineq` and ..." (where `ineq` is
    either "`js` < `js`" or "`js` > `js` for some variables `js`")
    """
    #formula_list = []
    #for ineq in string_formula.replace(' < ',' <= ').replace(' > ',' >= ').split('and'):
    #    if ineq.find(' = ') != -1: # if ineq is an Eq
    #        ineq = 'Eq('+ ineq.replace(' = ',',') +')'
    #    formula_list.append( parse_expr(ineq) )
    #return formula_list
    return string_formula.replace(' < ',' <= ').replace(' > ',' >= ').split('and')

def sample_from_inequalities( ineqs_list, vars, verbose=False ):
    sample  = None
    if 'false' in ineqs_list: return None # system is inconsistent
    with redcas.ReduceCAS(verbose=verbose) as red:
        sample  = red.sample_from_inequalities( ineqs_list, vars )
    return sample

def get_ranges_for_inequalities( ineqs_list, verbose=False ):
    with redcas.ReduceCAS(verbose=verbose) as red:
        result = red.get_ranges_for_inequalities( ineqs_list )
    return result

def ranges_and_eqs_to_strs( eqs_and_ranges ):
    return [
        str(ks) + " = " + str(e)
        for ks, e in eqs_and_ranges
    ]
