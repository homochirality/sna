# -*- coding: utf-8 -*-

# Copyright 2017 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function
from functools import reduce
from collections import OrderedDict
import sys

class ReactionsDetails(object):
    """
    Creates an object which contains multiple important attributes from the reactions
    passed to it.
    self.reactions : passed reactions
    self.species   : species found or passed to object
    self.stoichiometric    or self.S : stoichiometric matrix
    self.reactions_order   or self.K : reactions order matrix
    self.velocity_function or self.R : velocity function vector
    self.species_symbols   or self.xs : symbols used for each one of the species (x0, x1, ...)
    self.reactions_symbols or self.ks : symbols used for each one of the rates (k0, k1, ...)
    """
    def __init__(self, reactions, species=None, sort_by=None):
        self.__duals = None
        self.__reactions_str = None
        self.__reactions_andres_carolina = None

        reactions = self.__reactions_to_dicts(reactions)

        self.reactions = []
        # removing duplicate reactions:
        for r in reactions:
            if r not in self.reactions:
                self.reactions.append( r )

        # getting species
        species_from_reactions = self.__extract_species(self.reactions)
        if species is None:
            self.species = species_from_reactions
        else:
            msg = ( "Species passed don't coincide with species on reactions\n" +
                    "species from reactions: {}\n".format( set(species_from_reactions) ) +
                    "species passed (not in order): {}\n".format( set(species) )
            )
            assert set(species) == set(species_from_reactions), msg
            self.species = species

        if sort_by is not None:
            if sort_by == 'andres&carolina':
                #print(self.reactions)
                self.reactions = self.__sort_by_andres_carolina(self.reactions, species)
                #print(self.reactions)
                #print(self.reactions_andrescarolina)

            else:
                raise NotImplementedError('At the moment, there is only two sortings for reactions,'
                                        + "either don't order (sort_by==None), or sort_by=='andres&carolina'")

        S, K      = self.__generate_SNA_matrices(self.reactions, self.species)
        R, xs, ks = self.__generate_velocity_function(K)

        self.stoichiometric    = self.S  = S
        self.reactions_order   = self.K  = K
        self.velocity_function = self.R  = R
        self.species_symbols   = self.xs = xs
        self.reactions_symbols = self.ks = ks

    def __reactions_to_dicts(self, reacts):
        import re

        arrow_re = re.compile(r'<?->')
        def to_dict(reaction):
            left, right = arrow_re.split( reaction )
            start, end  = arrow_re.search( reaction ).span()
            left  = species_w_amounts_to_dict(left)
            right = species_w_amounts_to_dict(right)
            if end - start == 2: # ->
                return [(left, right)]
            else: # <->
                return [(left, right), (right, left)]

        aSpecies = r'([a-zA-Z][a-zA-Z0-9_-]*)'
        amount   = r'([0-9]*)'
        aSpecies_w_amount = re.compile(amount+r'\s*'+aSpecies)
        def species_w_amounts_to_dict(swas):
            toRet = {}
            for (a, s) in aSpecies_w_amount.findall(swas):
                a = 1 if a=='' else int(a)
                if not s in toRet:
                    toRet[s] = a
                else:
                    toRet[s] += a
            return toRet

        toRet = []
        for reaction in reacts:
            toRet.extend( to_dict(reaction) )
        return toRet

    def __extract_species(self, reactions):
        species = []
        for reaction in reactions:
            left, right = reaction
            species_in_reaction = list(left.items())+list(right.items())
            for (aSpecies, amount) in species_in_reaction:
                if not aSpecies in species:
                    species.append( aSpecies )
        return species

    def __reactions_from_dicts_to_tuples(self, reactions, species):
        #print("HEre")
        #print(reactions)
        #print(species)
        side2tuple = lambda side: tuple( [side[s] if s in side else 0 for s in species] )
        return [(side2tuple(r[0]), side2tuple(r[1])) for r in reactions]

    def get_duals(self):
        if self.__duals is not None:
            return self.__duals

        reactions_as_tuples = self.__reactions_from_dicts_to_tuples(self.reactions, self.species)
        duals_not_found = set( range(len(reactions_as_tuples)) )
        duals = []
        for r in range(len(reactions_as_tuples)):
            reaction = reactions_as_tuples[r]
            dual = self.__dual_pair_for(reaction)
            if dual in reactions_as_tuples:
                dual_index = reactions_as_tuples.index(dual)
                if dual_index > r:
                    duals.append( (r, dual_index) )
                    duals_not_found.remove( r )
                    duals_not_found.remove( dual_index )

        self.__duals = duals, sorted(duals_not_found)
        return self.__duals
        #return [(self.ks[f], self.ks[s]) for (f,s) in duals], [self.ks[n] for n in sorted(duals_not_found)]

    def __dual_pair_for( self, reaction ):
        """ The reaction is represented a 2-tuple of tuples, each tuple
        contains in order: the homoquiral species on positions 0 and 1, the rest
        of species from position 2 and onward"""
        left, right = reaction
        return (left[1],left[0])+left[2:] , (right[1],right[0])+right[2:]

    def __sort_by_andres_carolina(self, reactions, species):
        def extract_dual( reactions_as_tuples, reaction ):
            dual = self.__dual_pair_for(reaction)
            if dual in reactions_as_tuples:
                reacts2ret = reactions_as_tuples[:] # copying list
                reacts2ret.pop( reactions_as_tuples.index(dual) )
                return racts2ret, dual
            else:
                return reactions_as_tuples, None

        def group_by_duals( reactions_as_tuples ):
            duals_not_found = set( range(len(reactions_as_tuples)) )
            duals = []
            for r in range(len(reactions_as_tuples)):
                reaction = reactions_as_tuples[r]
                dual = self.__dual_pair_for(reaction)
                if dual in reactions_as_tuples:
                    dual_index = reactions_as_tuples.index(dual)
                    if dual_index > r:
                        duals.append( (r, dual_index) )
                        duals_not_found.remove( r )
                        duals_not_found.remove( dual_index )
            return duals, sorted(duals_not_found)

        def test_productive(reaction):
            left, right = reaction
            return left[0]==0 and left[1]==0 and (right[0]+right[1]==1)
        def test_destructive(reaction):
            left, right = reaction
            return right[0]==0 and right[1]==0 and (left[0]+left[1]==1)
        def test_autocatalytic(reaction):
            left, right = reaction
            return (
                (left[0]==0 and right[0]==0 and left[1]==1 and right[1]==2)
                or
                (left[0]==1 and right[0]==2 and left[1]==0 and right[1]==0)
            )
        def test_decomposition(reaction):
            left, right = reaction
            return (
                (left[0]==0 and right[0]==0 and left[1]==2 and right[1]==1)
                or
                (left[0]==2 and right[0]==1 and left[1]==0 and right[1]==0)
            )
        def test_enantio_selective(reaction):
            left, right = reaction
            return right[0]==1 and right[1]==1 and (left[0]+left[1]==1)
        def test_collision(reaction):
            left, right = reaction
            return left[0]==1 and left[1]==1

        #print("hoho")
        #print(reactions)
        reactions_as_tuples = self.__reactions_from_dicts_to_tuples(reactions, species)
        duals, not_duals = group_by_duals( reactions_as_tuples )

        productive        = []
        destructive       = []
        autocatalytic     = []
        decomposition     = []
        enantio_selective = []
        collision         = []
        i = 0
        while i < len(duals):
            dual_l, dual_r = duals[i]
            reactuple_left = reactions_as_tuples[dual_l]
            dual_reacts    = (reactions[dual_l], reactions[dual_r])
            #print("here")
            #print(reactuple_left)
            if   test_productive        ( reactuple_left ): productive       .append( dual_reacts )
            elif test_destructive       ( reactuple_left ): destructive      .append( dual_reacts )
            elif test_autocatalytic     ( reactuple_left ): autocatalytic    .append( dual_reacts )
            elif test_decomposition     ( reactuple_left ): decomposition    .append( dual_reacts )
            elif test_enantio_selective ( reactuple_left ): enantio_selective.append( dual_reacts )
            elif test_collision         ( reactuple_left ): collision        .append( dual_reacts )
            else:
                i+=1
                continue
            duals.pop(i)

        # detecting if there is any self-dual (collision) reaction
        i = 0
        while i < len(not_duals):
            r_index = not_duals[i]
            if test_collision( reactions_as_tuples[r_index] ):
                collision.append( (reactions[r_index],) )
                not_duals.pop(i)
            else:
                i+=1

        others = [(reactions[r1],reactions[r2]) for (r1,r2) in duals] + [(reactions[r],) for r in not_duals]

        self.reactions_andrescarolina = OrderedDict([
            ('productive', productive),
            ('destructive', destructive),
            ('autocatalytic', autocatalytic),
            ('decomposition', decomposition),
            ('enantio-selective', enantio_selective),
            ('collision', collision),
            ('others', others)
        ])

        if len(others) > 0:
            msg_error = ("!! Warning from sorting by andres&carolina algorithm\n"
                        +"!! Warning: ignoring reactions: {}\n".format( [(self.reaction_to_str(r[0]),) for r in others] )
                        +"!! The ignored reactions are neither of any type known (productive, enantio-selective, ...)\n"
                        +"!! or they have no dual pair\n")
            print(msg_error, file=sys.stderr)

        return reduce( # new order of reactions
            lambda x,y: x+list(y)
          ,
            productive +
            destructive +
            autocatalytic +
            decomposition +
            enantio_selective +
            collision
          ,
            []
        )

    def __generate_SNA_matrices(self, reactions, species):
        import numpy as np
        S = np.zeros( (len(species), len(reactions)), dtype='int' )
        K = np.zeros( (len(species), len(reactions)), dtype='int' )

        for i in range(len(reactions)):
            left, right = reactions[i]
            for (aSpecies, amount) in left.items():
                S[species.index(aSpecies)][i] -= amount
                K[species.index(aSpecies)][i] += amount

            for (aSpecies, amount) in right.items():
                S[species.index(aSpecies)][i] += amount

        return S, K

    def __generate_velocity_function(self, K):
        import sympy
        #num_species, num_reactions = K.shape
        num_species, num_reactions = len(self.species), len(self.reactions)
        xs = sympy.symbols( 'x0:%d' % num_species )
        ks = sympy.symbols( 'k0:%d' % num_reactions )
        R = sympy.ones( num_reactions, 1 )
        for r in range(num_reactions):
            R[r] *= ks[r]
            for s in range(num_species):
                if K[s][r] > 1:
                    R[r] *= xs[s]**K[s][r]
                elif K[s][r] > 0: ## i.e. K[s][r] == 1
                    R[r] *= xs[s]
        return R, xs, ks

    def reaction_to_str(self, reaction):
        def to_str(d):
            return ' + '.join( ['{} {}'.format(d[s], s) if d[s]>1 else s for s in self.species if s in d] )
        l,r = reaction
        return to_str(l)+' -> '+to_str(r)

    def get_reactions(self):
        if self.__reactions_str is not None:
            return self.__reactions_str

        toRet = []
        for reaction in self.reactions:
            toRet.append( self.reaction_to_str(reaction) )

        self.__reactions_str = toRet
        return toRet

    def get_reactions_andrescarolina(self):
        if self.__reactions_andres_carolina is not None:
            return self.__reactions_andres_carolina

        toRet = OrderedDict()
        for type, reactions in self.reactions_andrescarolina.items():
            if type not in toRet:
                toRet[type] = []
            for r_tuple in reactions:
                toRet[type].append( tuple( self.reaction_to_str(r) for r in r_tuple ) )

        self.__reactions_andres_carolina = toRet
        return toRet

#ReactionsDetails(['a <-> b'])
# a = ReactionsDetails(['x -> y', 'z -> 2y'])
# a = ReactionsDetails(['x + 2y -> 3y', 'x + 2z -> 3z', 'x + y + z <-> 3x'])

def sna_matrices(reactions, species = None):
    rd = ReactionsDetails(reactions, species)
    return (
        rd.stoichiometric,
        rd.reactions_order,
        rd.velocity_function,
        rd.species_symbols,
        rd.reactions_symbols
    )
